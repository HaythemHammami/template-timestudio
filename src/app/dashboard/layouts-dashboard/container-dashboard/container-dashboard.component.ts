import { Component, OnInit } from '@angular/core';
declare var dashboardjs: any; //Declare js function
declare var sidemenu: any; //Declare js function

@Component({
  selector: 'app-container-dashboard',
  templateUrl: './container-dashboard.component.html',
  styleUrls: ['./container-dashboard.component.css']
})
export class ContainerDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    dashboardjs()
    sidemenu()
  }

}
