import { Component, OnInit } from '@angular/core';
declare var dashboardjs: any; //Declare js function
declare var sidemenu: any; //Declare js function

@Component({
  selector: 'app-test-dashboard',
  templateUrl: './test-dashboard.component.html',
  styleUrls: ['./test-dashboard.component.css']
})
export class TestDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    dashboardjs();
    sidemenu();
  }

}
