import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './layout-visualization/footer/footer.component';
import { HeaderComponent } from './layout-visualization/header/header.component';
import { ContainerComponent } from './layout-visualization/container/container.component';
import { IndexComponent } from './layout-visualization/layout-container/index/index.component';
import { AboutUsComponent } from './layout-visualization/layout-container/about-us/about-us.component';
import { PortfilioComponent } from './layout-visualization/layout-container/portfilio/portfilio.component';
import { ContactusComponent } from './layout-visualization/layout-container/contactus/contactus.component';
import { MeetingComponent } from './layout-visualization/layout-container/meeting/meeting.component';
import { TestDashboardComponent } from './dashboard/test-dashboard/test-dashboard.component';
import { WelcomePageComponent } from './dashboard/container-dashboard/welcome-page/welcome-page.component';
import { ContainerDashboardComponent } from './dashboard/layouts-dashboard/container-dashboard/container-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ContainerComponent,
    IndexComponent,
    AboutUsComponent,
    PortfilioComponent,
    ContactusComponent,
    MeetingComponent,
    TestDashboardComponent,
    WelcomePageComponent,
    ContainerDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
