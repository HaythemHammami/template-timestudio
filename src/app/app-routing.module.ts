import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerComponent } from './layout-visualization/container/container.component';
import { IndexComponent } from './layout-visualization/layout-container/index/index.component';
import { AboutUsComponent } from './layout-visualization/layout-container/about-us/about-us.component';
import { PortfilioComponent } from './layout-visualization/layout-container/portfilio/portfilio.component';
import { ContactusComponent } from './layout-visualization/layout-container/contactus/contactus.component';
import { MeetingComponent } from './layout-visualization/layout-container/meeting/meeting.component';
import { TestDashboardComponent } from './dashboard/test-dashboard/test-dashboard.component';
import { ContainerDashboardComponent } from './dashboard/layouts-dashboard/container-dashboard/container-dashboard.component';
import { WelcomePageComponent } from './dashboard/container-dashboard/welcome-page/welcome-page.component';


const routes: Routes = [{
  path: "",
  component: ContainerComponent,
  children: [
    {        
      path: "",
      component: IndexComponent,
    },{        
      path: "about-us",
      component: AboutUsComponent,
    },{        
      path: "portfolio",
      component: PortfilioComponent,
    },{        
      path: "contact",
      component: ContactusComponent,
    },{        
      path: "meeting",
      component: MeetingComponent,
    }]
},{
  path: "welcome",
  component: ContainerDashboardComponent,
  children: [
    {        
      path: "",
      component: WelcomePageComponent,
    }
  ]
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
